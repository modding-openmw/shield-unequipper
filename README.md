# Shield Unequipper

Un-equips the shield when equipping any two-handed weapon. You no longer can benefit from the armor rating of the shield while both hands are occupied.

This was originally created during a modding-openmw.com live stream, you can watch that [here](https://www.youtube.com/watch?v=hQo1_koFWcs).

#### Credits

Author: **johnnyhostile**

**Special Thanks**:

* **The OpenMW team, including every contributor** for making OpenMW and OpenMW-CS
* **The Modding-OpenMW.com team** for being amazing
* **All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server** for their dilligent testing <3
* **Bethesda** for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Localization

DE: Atahualpa

EN: johnnyhostile, Atahualpa

PT_BR: Hurdrax Custos

SV: Lysol

#### Web

[Project Home](https://modding-openmw.gitlab.io/shield-unequipper/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/shield-unequipper)

#### Installation

**OpenMW 0.48 or newer is required!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/shield-unequipper/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Gameplay\shield-unequipper

        # Linux
        /home/username/games/OpenMWMods/Gameplay/shield-unequipper

        # macOS
        /Users/username/games/OpenMWMods/Gameplay/shield-unequipper

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Gameplay\shield-unequipper"`)
1. Add `content=shield-unequipper.omwscripts` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher

#### Lua Interface

It's possible another mod might conflict with this one and need to temporarily ask to disable it. To that end, a `Disable()` interface function is provided that will disable this mod for one second. You can use it in another mod like this:

```lua
if I.ShieldUnequipper then
    I.ShieldUnequipper.Disable()
end
```

A scenario where this might be useful is if your mod is trying to equip something; sometimes that sort of thing can cause a jarring interaction so temporarily disabling this mod is a simple way to make them work together.

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/shield-unequipper/-/issues)
* Email `shield-unequipper@modding-openmw.com`
* Contact the author on Discord: `@johnnyhostile`
* Contact the author on Libera.chat IRC: `johnnyhostile`
