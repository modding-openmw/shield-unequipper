--
-- Turn file links like `CHANGELOG.md` into URLs like `/changelog/`
--
-- Minimum soupault version: 4.3.0
-- Author: Hristos N. Triantafillou
-- License: MIT
--
Plugin.require_version("4.3.0")

local things = HTML.select_all_of(page, {"a"})

local n = 1
local c = size(things)
while (n <= c) do
    local thing = things[n]

    local href = HTML.get_attribute(thing, "href")
    if href ~= nil then
        if href == "CHANGELOG.md" then
            HTML.set_attribute(thing, "href", "/changelog/")
        end
        if href == "WALKTHROUGH.md" then
            HTML.set_attribute(thing, "href", "/walkthrough/")
        end
    end

    n = n + 1
end
