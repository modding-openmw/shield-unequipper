local async = require("openmw.async")
local core = require("openmw.core")
local self = require("openmw.self")
local storage = require('openmw.storage')
local Player = require("openmw.types").Player
local Weapon = require("openmw.types").Weapon
local I = require('openmw.interfaces')

local relevantTypes = {
    [Weapon.TYPE.AxeTwoHand] = true,
    [Weapon.TYPE.BluntTwoClose] = true,
    [Weapon.TYPE.BluntTwoWide] = true,
    [Weapon.TYPE.LongBladeTwoHand] = true,
    [Weapon.TYPE.MarksmanBow] = true,
    [Weapon.TYPE.MarksmanCrossbow] = true,
    [Weapon.TYPE.SpearTwoWide] = true
}
local shieldSlot = Player.EQUIPMENT_SLOT.CarriedLeft
local wpnSlot = Player.EQUIPMENT_SLOT.CarriedRight
local MOD_ID = "ShieldUnequipper"
local settingsKey = "SettingsPlayer" .. MOD_ID
local settings = storage.playerSection(settingsKey)

local lastShield
local unequipped

local lastFrameShield
local lastFrameRelevantWeapon

local disabled = false
local TEMP_DISABLE_SECONDS = 1

I.Settings.registerPage {
    key = MOD_ID,
    l10n = MOD_ID,
    name = 'name',
    description = 'description'
}

I.Settings.registerGroup {
    key = settingsKey,
    l10n = MOD_ID,
    name = "settingsTitle",
    page = MOD_ID,
    description = "settingsDesc",
    permanentStorage = false,
    settings = {
        {
            key = "rememberShield",
            name = "rememberShield_name",
            description = "rememberShield_desc",
            default = true,
            renderer = "checkbox"
        },
        {
            key = "debugMsgs",
            name = "debugMsgs_name",
            description = "debugMsgs_desc",
            default = false,
            renderer = "checkbox"
        }
    }
}

-- Prevent frequent settings lookups by storing values that
-- are in the hot path and then updating them as needed
local rememberShield = settings:get("rememberShield")
local function updateRememberShield(_, key)
    if key == "rememberShield" then
        rememberShield = settings:get("rememberShield")
    end
end
settings:subscribe(async:callback(updateRememberShield))

local debugMsgs = settings:get("debugMsgs")
local function updateDebugMsgs(_, key)
    if key == "debugMsgs" then
        debugMsgs = settings:get("debugMsgs")
    end
end
settings:subscribe(async:callback(updateDebugMsgs))

local function onFrame()
    if disabled == true then return end
    local equipped = Player.equipment(self)
    local equippedWpn = equipped[wpnSlot]
    local shieldEquipped = equipped[shieldSlot]
    local relevantWeaponEquipped
    if equippedWpn and Weapon.objectIsInstance(equippedWpn) then
        relevantWeaponEquipped = relevantTypes[Weapon.record(equippedWpn).type]
    end

    -- Try to remember the last equipped shield
    if rememberShield
        and shieldEquipped
        and lastShield ~= shieldEquipped
    then
        if lastShield and debugMsgs then
            print(string.format("[%s]: Remembering shield: %s", MOD_ID, lastShield.recordId))
        elseif debugMsgs then
            print(string.format("[%s]: No shield to remember", MOD_ID))
        end
        lastShield = shieldEquipped
    end

    -- Remove the shield if no lastFrameRelevantWeapon
    if equippedWpn
        and relevantWeaponEquipped
        and shieldEquipped
        and lastFrameShield
        and not unequipped
    then
        if debugMsgs then
            print(string.format("[%s]: Removing shield: %s", MOD_ID, shieldEquipped.recordId))
        end
        equipped[shieldSlot] = nil
        Player.setEquipment(self, equipped)
        unequipped = true
    end

    -- Remove the relevant weapon if no lastFrameShield
    if equippedWpn
        and relevantWeaponEquipped
        and lastFrameRelevantWeapon
        and shieldEquipped
        and not lastFrameShield
    then
        if debugMsgs then
            print(string.format("[%s]: Removing weapon: %s", MOD_ID, equippedWpn.recordId))
        end
        equipped[wpnSlot] = nil
        Player.setEquipment(self, equipped)
        unequipped = false
    end

    local lastShieldCount = 0
    if lastShield and lastShield:isValid() then
        if core.API_REVISION >= 51 then
            lastShieldCount = #Player.inventory(self):findAll(lastShield.recordId)
        else
            lastShieldCount = Player.inventory(self):countOf(lastShield.recordId)
        end
    end

    -- Re-equip the shield as needed
    if rememberShield
        and lastShield
        and lastShieldCount > 0
        and unequipped
        and not shieldEquipped
        and not relevantWeaponEquipped
    then
        if debugMsgs then
            print(string.format("[%s]: Re-equipping shield: %s", MOD_ID, lastShield.recordId))
        end
        equipped[shieldSlot] = lastShield
        Player.setEquipment(self, equipped)
        unequipped = false
    end

    -- Record info for use on the next frame
    lastFrameShield = equipped[shieldSlot]
    if equippedWpn and Weapon.objectIsInstance(equippedWpn) then
        lastFrameRelevantWeapon = relevantTypes[Weapon.record(equippedWpn).type]
    else
        lastFrameRelevantWeapon = nil
    end
end

local function onLoad(data)
    lastShield = data.lastShield
    unequipped = data.unequipped
end

local function onSave()
    return {
        lastShield = lastShield,
        unequipped = unequipped
    }
end

local disableCallback = async:registerTimerCallback(
    "disableCallback",
    function() disabled = false end
)
local function disable()
    disabled = true
    async:newSimulationTimer(TEMP_DISABLE_SECONDS, disableCallback)
end

return {
    engineHandlers = {
        onFrame = onFrame,
        onLoad = onLoad,
        onSave = onSave
    },
    interface = {
        Disable = disable,
        version = 1
    },
    interfaceName = MOD_ID
}
